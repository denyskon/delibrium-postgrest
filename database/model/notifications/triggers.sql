drop trigger if exists update_noitifications_change_at on aula.notifications;
create trigger update_noitifications_change_at before update on aula.notifications for each row execute procedure aula.update_changed_column();
