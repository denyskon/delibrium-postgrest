create or replace function aula.delete_user_bulk(schoolid bigint, userids bigint[])
  returns void
  language plpython3u
  set search_path = public, aula
as $$
import json

current_user_id = plpy.execute("select current_setting('request.jwt.claim.user_id', true)")[0]['current_setting']

if current_user_id in userids:
  return

user_id_list = ",".join(map(str, userids))
login_ids = list(map(lambda x: str(x['user_login_id']), plpy.execute('SELECT user_login_id FROM aula.users WHERE id IN ({})'.format(user_id_list))))

plpy.execute("UPDATE aula.users SET "+
      "user_login_id = null,"+
      "first_name = 'deleted',"+
      "last_name = 'user',"+
      "username = 'deleted-' || id,"+
      "config = json_build_object('deleted', to_jsonb('t'::boolean)),"+
      "email = '',"+
      "picture = ''"+
      " WHERE "+
      "id IN ({});".format(user_id_list))
plpy.execute('DELETE FROM aula_secure.user_login WHERE id IN ({})'.format(",".join(login_ids)))
plpy.execute('DELETE FROM aula.user_group WHERE user_id IN ({})'.format(user_id_list))
return
$$;
