create or replace function aula.remove_user_roles_bulk(schoolid bigint, userids bigint[], groupid aula.group_id, ideaspace bigint)
  returns boolean
  language plpython3u
  set search_path = public, aula
  as $$

user_id_list = ",".join(map(str, userids))

remove_string = "DELETE FROM aula.user_group WHERE "
remove_string += "school_id={} AND idea_space={} AND group_id='{}' AND user_id IN ({})".format(schoolid, ideaspace, groupid, user_id_list)

plpy.execute(remove_string)

return True
$$;
