#!/bin/bash


while getopts h:p:u:d:c: option
do
case "${option}"
in
u) DBUSER="-U ${OPTARG} --password";;
h) DBHOST="-h ${OPTARG}";;
p) DBPORT="-p ${OPTARG}";;
d) DATABASE=${OPTARG};;
c) CONFIG=${OPTARG};;
esac
done

env base_dir=$(pwd) $(grep -v '^#' $CONFIG) psql $DBPORT $DBHOST $DBUSER -f database/init.sql $DATABASE
